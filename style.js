import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  headerText: {
    paddingTop: 80,
    fontSize: 50,
    marginBottom: 30
  },
  input: { 
    marginTop: 20,
    borderColor: "black",
    borderWidth: 2,
    borderRadius: 5,
    width: 300,
    paddingVertical: 20,
    paddingLeft: 10,
    fontSize: 20
  },
  errorText: {
    color: "red",
    fontSize: 12
  },
  image: {
    width: 200,
    height: 200
  },
  statusText: {
      padding: 20,
      marginTop: 90,
  }
});
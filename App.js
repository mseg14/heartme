import React from 'react';
import { Text, View, TextInput, Image, TouchableOpacity } from 'react-native';
import { bloodTestConfig } from './BloodTestConfig'
import { validateBloodTestCategory, validateResult } from './validators'
import { statuses } from './consts'
import {styles} from './style'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: null,
      statusInfoText: null,
      result: null,
      resultError: false,
      bloodTestCategory: null,
      bloodTestCategoryError: false,
    };
  }

  getRequiredBloodTests = () => {
    var inputSplitIntoWords = this.state.bloodTestCategory.split(' ').map(word => word.toLowerCase())
    var potentialBloodTests = bloodTestConfig.bloodTestConfig.map(item => item.name.toLocaleLowerCase().split(' ')[0])
    return potentialBloodTests.filter(value => inputSplitIntoWords.includes(value))
  }

  submitForm = () => {
    if (!this.state.result){
      this.setState({resultError: true})
    }

    if (!this.state.bloodTestCategory){
      this.setState({bloodTestCategoryError: true})
    }

    if (this.state.bloodTestCategory && this.state.result && !this.state.resultError && !this.state.bloodTestCategoryError){
      var intersectionOfTests = this.getRequiredBloodTests()
      if (intersectionOfTests.length < 1){
        this.setState({status: statuses.UNKNOWN})
        this.setState({statusInfoText: 'Unknown test'})
      } else if ( intersectionOfTests.length > 1) {
        this.setState({status: null})
        this.setState({statusInfoText: "Please enter one type of blood test"})
      } else {
        var threshold = bloodTestConfig.bloodTestConfig.filter(test => test.name.split(' ')[0].toLocaleLowerCase() == intersectionOfTests[0].toLocaleLowerCase())[0].threshold
        var testResult = parseFloat(this.state.result) < parseFloat(threshold)
        if (testResult){
          this.setState({status: statuses.GOOD})
          this.setState({statusInfoText: "You got great results on your " + intersectionOfTests[0].toUpperCase() + " test"})
        } else {
          this.setState({status: statuses.BAD})
          this.setState({statusInfoText: "You got poor results on your " + intersectionOfTests[0].toUpperCase() + " test, please contact your phisician"})
        }
      }
    }
  }

  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.headerText}>Am I OK?</Text>
        <View>
          <TextInput 
            style={styles.input}
            placeholder="Test Name"
            placeholderTextColor="black"
            onChangeText={(bloodTestCategory) => {
              if (this.state.status){
                this.setState({status: null})
                this.setState({statusInfoText: null})
              }
              this.setState({ bloodTestCategory }); 
              if (bloodTestCategory.length == 0){
                this.setState({bloodTestCategoryError: false})
                return
              }
              if (!validateBloodTestCategory(bloodTestCategory)){
                this.setState({bloodTestCategoryError: true})
              } else {
                this.setState({bloodTestCategoryError: false})
              }
            } 
          }>
          </TextInput>
          { this.state.bloodTestCategoryError && <Text style={styles.errorText}>Can only use a-zA-Z0-9(),-:/!</Text>}
          { !this.state.bloodTestCategoryError && <Text style={styles.errorText}> </Text>}
        </View>
        <View>
        <TextInput 
          style={styles.input}
          placeholder="Result"
          placeholderTextColor="black"
          keyboardType="number-pad"
          onChangeText={(result) => {
            if (this.state.status){
              this.setState({status: null})
              this.setState({statusInfoText: null})
            }
            this.setState({ result })
            if (result.length == 0){
              this.setState({resultError: false})
              return
            }
            if (!validateResult(result)){
              this.setState({resultError: true})
            } else {
              this.setState({resultError: false})
            }
          }}>
        </TextInput>
        { this.state.resultError && <Text style={styles.errorText}>Can only use digits and a single dot</Text>}
        { !this.state.resultError && <Text style={styles.errorText}> </Text>}
        </View>
        <Text style={styles.statusText}>
          {this.state.statusInfoText}
        </Text>
        { this.state.status == statuses.GOOD && 
            <Image
              source={require('./assets/happyHeart.png')}
              style={styles.image}>
            </Image>}
        { this.state.status == statuses.BAD && 
            <Image
              source={require('./assets/sadHeart.png')}
              style={styles.image}>
            </Image>}
        { this.state.status == statuses.UNKNOWN &&             
            <Image
              source={require('./assets/heartbroken.png')}
              style={styles.image}>
            </Image>}
        { this.state.status == null && 
          <TouchableOpacity
            onPress={this.submitForm}>
            <Image
              source={require('./assets/heart.png')}
              style={styles.image}>
            </Image>
          </TouchableOpacity>}
      </View>
    )
  }
}

export function validateBloodTestCategory (bloodTestCategory) {
    const bloodTestCategoryRegex = /[a-zA-Z0-9(),-:/! ]+$/gm;
      return bloodTestCategoryRegex.test(bloodTestCategory);
  };

export function validateResult (result) {
    const resultRegex = /^[0-9]+\.{0,1}[0-9]*$/mg;
      return resultRegex.test(result);
  };